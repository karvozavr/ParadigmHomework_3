#!/usr/bin/env python3

import numpy as np
import sys


def strass(A, B):
    size = A.shape[0]

    if size == 1:
        return A * B
    if size % 2 != 0:
        A = np.insert(A, size, 0, axis=0)
        A = np.insert(A, size, 0, axis=1)
        B = np.insert(B, size, 0, axis=0)
        B = np.insert(B, size, 0, axis=1)

    a11, a12, a21, a22 = split_matrix(A)
    b11, b12, b21, b22 = split_matrix(B)

    r1 = strass(a11 + a22, b11 + b22)
    r2 = strass(a21 + a22, b11)
    r3 = strass(a11, b12 - b22)
    r4 = strass(a22, -b11 + b21)
    r5 = strass(a11 + a12, b22)
    r6 = strass(-a11 + a21, b11 + b12)
    r7 = strass(a12 - a22, b21 + b22)

    upper_part = np.hstack((r1 + r4 - r5 + r7, r3 + r5))
    down_part = np.hstack((r2 + r4, r1 + r3 - r2 + r6))
    return np.vstack((upper_part, down_part))[:size, :size]


def split_matrix(mat):
    upper_part, bottom_part = np.vsplit(mat, 2)
    m11, m12 = np.hsplit(upper_part, 2)
    m21, m22 = np.hsplit(bottom_part, 2)
    return m11, m12, m21, m22


def read_matrix(n):
    mat = np.zeros((n, n), dtype=int)
    for i in range(n):
        mat[i] = np.array(input().split())
    return mat


def print_matrix(mat):
    for line in mat:
        print(*line)


def main():
    n = int(input())
    A = read_matrix(n)
    B = read_matrix(n)
    print_matrix(strass(A, B)[:n, :n])


if __name__ == '__main__':
    main()
